export const environment = {
    production: true,

    region: 'ap-southeast-1',

    identityPoolId: 'ap-southeast-1:2cbba552-2dc4-4219-b1cf-ce7ab4c4469b',
    userPoolId: 'ap-southeast-1_8ppAxvS41',
    clientId: '6rcshvno2b304ukbskdl6j8i3b',

    rekognitionBucket: 'vapefy-10272019',
    albumName: "usercontent",
    bucketRegion: 'ap-southeast-1',

    ddbTableName: 'vapefy_ddb_10272019',

    cognito_idp_endpoint: '',
    cognito_identity_endpoint: '',
    sts_endpoint: '',
    dynamodb_endpoint: '',
    s3_endpoint: ''

};

